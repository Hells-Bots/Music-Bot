discord.py[voice] ~= 0.16.12
youtube_dl == 2018.11.7
colorlog ~= 3.1.0
cffi --only-binary all; sys_platform == 'win32'
