import asyncio
import inspect
import logging
import os
import pathlib
import random
import shutil
import sys
import time
import traceback
from collections import defaultdict
from textwrap import dedent
from typing import Union

import aiohttp
import colorlog
import discord
from discord import Game
from discord.ext.commands.bot import _get_variable

import MusicBot
from MusicBot.audio import downloader
from MusicBot.audio.opus_loader import load_opus_lib
from MusicBot.audio.playlist import Playlist
from MusicBot.audio.entry import StreamPlaylistEntry, URLPlaylistEntry
from MusicBot.audio.player import MusicPlayer
from MusicBot.common_types import exceptions
from MusicBot.common_types.responses import Response
from MusicBot.common_types.voice_state_update import VoiceStateUpdate
from MusicBot.common_types.skip_state import SkipState
from MusicBot.config_handlers.config import Config, ConfigDefaults
from MusicBot.config_handlers.permissions import Permissions, PermissionsDefaults
from MusicBot.extended_discord_client import ExtendedDiscordClient
from MusicBot.helpers.constants import AUDIO_CACHE_PATH
from MusicBot.helpers.constants import VERSION as BOTVERSION
from MusicBot.helpers.file_handling import load_file, write_file
from MusicBot.helpers.formaters import fixg


load_opus_lib()

log = logging.getLogger(__name__)


class MusicBotClient(ExtendedDiscordClient):
    def __init__(self, config_file=None, perms_file=None):
        if config_file is None:
            config_file = ConfigDefaults.options_file

        if perms_file is None:
            perms_file = PermissionsDefaults.perms_file

        self.players = {}
        self.exit_signal = None
        self.init_ok = False
        self.cached_app_info = None
        self.last_status = None

        self.config = Config(config_file)
        self.permissions = Permissions(perms_file, grant_all=[self.config.owner_id])

        self.blacklist = set(load_file(self.config.blacklist_file))
        self.autoplaylist = load_file(self.config.auto_playlist_file)

        self.aiolocks = defaultdict(asyncio.Lock)
        self.downloader = downloader.Downloader(download_folder='audio_cache')

        self._setup_logging()

        if not self.autoplaylist:
            log.warning("Auto-Playlist is empty, disabling.")
            self.config.auto_playlist = False
        else:
            log.info("Loaded auto-playlist with {} entries".format(len(self.autoplaylist)))

        if self.blacklist:
            log.debug("Loaded blacklist with {} entries".format(len(self.blacklist)))

        # TODO: Do these properly
        ssd_defaults = {
            'last_np_msg': None,
            'auto_paused': False,
            'availability_paused': False
        }
        self.server_specific_data = defaultdict(ssd_defaults.copy)

        super().__init__()
        self.aiosession = aiohttp.ClientSession(loop=self.loop)
        self.http.user_agent += ' MusicBot/%s' % BOTVERSION

    def __del__(self):
        try:
            self.http.session.close()
        except:
            log.warning('Error while closing the http session during deletion.')

        try:
            self.aiosession.close()
        except:
            log.warning('Error while closing the asyncio session during deletion.')

    # region Methods for Permission-Checking
    def ensure_owner_only(self):
        orig_msg = _get_variable('message')

        if not orig_msg.author.id == self.config.owner_id:
            raise exceptions.PermissionsError("Only the Bot-Owner can use this command", expire_in=30)

    def ensure_dev_only(self):
        orig_msg = _get_variable('message')

        if not orig_msg.author.id in self.config.dev_ids:
            raise exceptions.PermissionsError("Only a Dev-Users can use this command", expire_in=30)
    # endregion

    # region General Helper Functions
    async def ensure_appinfo(self):
        await self._cache_app_info()
        return

    async def edit_profile(self, **fields):
        return await super().edit_profile(**fields)

    def _setup_logging(self):
        if len(logging.getLogger(__package__).handlers) > 1:
            log.debug("Skipping logger setup, already set up")
            return

        shandler = logging.StreamHandler(stream=sys.stdout)
        shandler.setFormatter(colorlog.LevelFormatter(
            fmt={
                'DEBUG': '{log_color}[{levelname}:{module}] {message}',
                'INFO': '{log_color}{message}',
                'WARNING': '{log_color}{levelname}: {message}',
                'ERROR': '{log_color}[{levelname}:{module}] {message}',
                'CRITICAL': '{log_color}[{levelname}:{module}] {message}',

                'EVERYTHING': '{log_color}[{levelname}:{module}] {message}',
                'NOISY': '{log_color}[{levelname}:{module}] {message}',
                'VOICEDEBUG': '{log_color}[{levelname}:{module}][{relativeCreated:.9f}] {message}',
                'FFMPEG': '{log_color}[{levelname}:{module}][{relativeCreated:.9f}] {message}'
            },
            log_colors={
                'DEBUG': 'cyan',
                'INFO': 'white',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'bold_red',

                'EVERYTHING': 'white',
                'NOISY': 'white',
                'FFMPEG': 'bold_purple',
                'VOICEDEBUG': 'purple',
            },
            style='{',
            datefmt=''
        ))
        shandler.setLevel(self.config.debug_level)
        logging.getLogger(__package__).addHandler(shandler)

        log.debug("Set logging level to {}".format(self.config.debug_level_str))

        if self.config.debug_mode:
            dlogger = logging.getLogger('discord')
            dlogger.setLevel(logging.DEBUG)
            dhandler = logging.FileHandler(filename='logs/discord.log', encoding='utf-8', mode='w')
            dhandler.setFormatter(logging.Formatter('{asctime}:{levelname}:{name}: {message}', style='{'))
            dlogger.addHandler(dhandler)

    async def _reset_bound_channels_topic(self, channels):
        for c in channels:
            await self.safe_set_topic(c, ':zzz:')

    async def _join_startup_channels(self, channels):
        joined_servers = set()
        channel_map = {c.server: c for c in channels}

        def _autopause(player):
            if self.check_if_vc_is_empty(player.voice_client.channel):
                log.info("Initial autopause in empty channel")

                player.pause()
                self.server_specific_data[player.voice_client.channel.server]['auto_paused'] = True

        for server in self.servers:
            if server.unavailable or server in channel_map:
                continue

            if server.me.voice_channel:
                log.info("Found resumable voice channel {0.server.name}/{0.name}".format(server.me.voice_channel))
                channel_map[server] = server.me.voice_channel

        for server, channel in channel_map.items():
            if server in joined_servers:
                log.info("Already joined a channel in \"{}\", skipping".format(server.name))
                continue

            if channel and channel.type == discord.ChannelType.voice:
                log.info("Attempting to join {0.server.name}/{0.name}".format(channel))

                chperms = channel.permissions_for(server.me)

                if not chperms.connect:
                    log.info("Cannot join channel \"{}\", no permission.".format(channel.name))
                    continue

                elif not chperms.speak:
                    log.info("Will not join channel \"{}\", no permission to speak.".format(channel.name))
                    continue

                try:
                    player = await self.get_player(channel, create=True, deserialize=self.config.persistent_queue)
                    joined_servers.add(server)

                    log.info("Joined {0.server.name}/{0.name}".format(channel))

                    if player.is_stopped:
                        player.play()

                    if self.config.auto_playlist and not player.playlist.entries:
                        if self.config.auto_pause:
                            player.once('play', lambda player, **_: _autopause(player))

                except Exception:
                    log.debug("Error joining {0.server.name}/{0.name}".format(channel), exc_info=True)
                    log.error("Failed to join {0.server.name}/{0.name}".format(channel))

            elif channel:
                log.warning("Not joining {0.server.name}/{0.name}, that's a text channel.".format(channel))

            else:
                log.warning("Invalid channel thing: {}".format(channel))

    async def _cache_app_info(self, *, update=False):
        if not self.cached_app_info and not update and self.user.bot:
            log.debug("Caching app info")
            self.cached_app_info = await self.application_info()

        return self.cached_app_info

    async def generate_invite_link(self, *, permissions=discord.Permissions(70380544), server=None):
        self.ensure_appinfo()

        return discord.utils.oauth_url(self.cached_app_info.id, permissions=permissions, server=server)
    # endregion

    # region Audio Helper Functions
    def _delete_old_audio_cache(self, path=AUDIO_CACHE_PATH):
        try:
            shutil.rmtree(path)
            return True
        except:
            try:
                os.rename(path, path + '__')
                shutil.rmtree(path)
            except:
                os.rename(path + '__', path)
                return False

        return True

    def _init_player(self, player, *, server=None):
        player = player.on('play', self.on_player_play) \
            .on('resume', self.on_player_resume) \
            .on('pause', self.on_player_pause) \
            .on('stop', self.on_player_stop) \
            .on('finished-playing', self.on_player_finished_playing) \
            .on('entry-added', self.on_player_entry_added) \
            .on('error', self.on_player_error)

        player.skip_state = SkipState()

        if server:
            self.players[server.id] = player

        return player

    def get_player_in(self, server: discord.Server) -> MusicPlayer:
        return self.players.get(server.id)

    async def get_player(self, channel, create=False, *, deserialize=False) -> MusicPlayer:
        server = channel.server

        if deserialize:
            voice_client = await self.get_voice_client(channel)
            player = await self.deserialize_queue(server, voice_client)

            if player:
                log.debug("Created player via deserialization for server %s with %s entries", server.id,
                          len(player.playlist))
                # Since deserializing only happens when the bot starts, I should never need to reconnect
                return self._init_player(player, server=server)

        if server.id not in self.players:
            if not create:
                raise exceptions.CommandError(
                    'The bot is not in a voice channel.  '
                    'Use %ssummon to summon it to your voice channel.' % self.config.command_prefix)

            voice_client = await self.get_voice_client(channel)

            playlist = Playlist(self)
            player = MusicPlayer(self, voice_client, playlist)
            self._init_player(player, server=server)

        async with self.aiolocks[self.reconnect_voice_client.__name__ + ':' + server.id]:
            if self.players[server.id].voice_client not in self.voice_clients:
                log.debug("Reconnect required for voice client in {}".format(server.name))
                await self.reconnect_voice_client(server, channel=channel)

        return self.players[server.id]

    async def update_now_playing(self,
         entry: Union[StreamPlaylistEntry, URLPlaylistEntry], player = None,
         is_paused: bool = False, is_stopped: bool = False,
         force_as_message: bool = False, extra_text: str = ''
    ):
        """
        Updates the "now playing" message
        :param entry: The currently playing entry.
        :param player: The used music player.
        :param is_paused: If the playback is paused or not
        :param is_stopped: If the playback is stopped or not. If it is, the now playing message is deleted / set to sleeping.
        :param force_as_message: If the config setting to set it as channel topic should be ignored and the update should
                                  be sent as a message to the channel.
        :param extra_text: Optional text to append at the end of the now playing message
        """

        channel = entry.meta.get('channel', None)
        song_title = entry.title.replace('*', '')

        if channel:
            if (self.config.now_playing_as_topic
                and not force_as_message
            ):
                if not is_stopped:
                    msg = '{} **{}**{}'.format(
                        ':notes:' if not is_paused else ':pause_button:',
                        song_title,
                        ', added by %s' % player.current_entry.meta['author'].display_name if \
                            player.current_entry.meta.get('author', False) else ''
                    )
                    await self.safe_set_topic(channel, msg)
                else:
                    await self.safe_set_topic(channel, ':zzz:')
            else:
                last_np_msg = self.server_specific_data[channel.server]['last_np_msg']

                if not is_stopped:
                    # Format the message:
                    if player is not None:
                        voice_channel_name = player.voice_client.channel.name.replace('*', '')
                    else:
                        voice_channel_name = 'an unknown voice channel'

                    if self.config.now_playing_mentions:
                        newmsg = '{} - Your song **{}** is {} in {}'.format(
                            entry.meta['author'].mention,
                            song_title,
                            'now playing' if not is_paused else 'paused',
                            voice_channel_name
                        )
                    else:
                        newmsg = '{} in {}: **{}**{}'.format(
                            'Now playing' if not is_paused else 'Currently paused',
                            voice_channel_name,
                            song_title,
                            ', added by %s' % player.current_entry.meta['author'].display_name if\
                                player.current_entry.meta.get('author', False) else ''
                        )

                    newmsg = newmsg + extra_text

                    # Check if message should be edited or deleted:
                    if last_np_msg is not None:
                        if last_np_msg.channel != channel:
                            await self.safe_delete_message(last_np_msg)
                            self.server_specific_data[channel.server]['last_np_msg'] = None
                            last_np_msg = None
                        else:
                            async for lmsg in self.logs_from(channel, limit=1):
                                if lmsg != last_np_msg and last_np_msg:
                                    # = the last now playing message is too high up.
                                    # Lets delete the old one and use a fresh new one.
                                    await self.safe_delete_message(last_np_msg)
                                    self.server_specific_data[channel.server]['last_np_msg'] = None
                                    last_np_msg = None

                    # Send the message:
                    if last_np_msg is not None:
                        # Edit:
                        last_np_msg = self.server_specific_data[channel.server]['last_np_msg'] = await self.safe_edit_message(
                            last_np_msg, newmsg, send_if_fail=True
                        )
                    else:
                        # Send a new message:
                        last_np_msg = self.server_specific_data[channel.server]['last_np_msg'] = await self.safe_send_message(
                            channel, newmsg, suppress_delete=True
                        )

                    if self.config.now_playing_as_topic:
                        await asyncio.sleep(self.config.delete_delay)
                        await self.safe_delete_message(last_np_msg)
                        self.server_specific_data[channel.server]['last_np_msg'] = None

                else:  # = stop it, delete message
                    await self.safe_delete_message(last_np_msg)
                    self.server_specific_data[channel.server]['last_np_msg'] = None
    # endregion

    # region Audio Player Lifecycle Functions
    async def on_player_play(self, player, entry):
        player.skip_state.reset()

        await self.update_now_playing(entry, player)

        if not entry.meta.get('do_loop', False):
            await self.serialize_queue(player.voice_client.channel.server)

    async def on_player_pause(self, player, entry, **_):
        await self.update_now_playing(entry, player, is_paused=True)

    async def on_player_resume(self, player, entry, **_):
        await self.update_now_playing(entry, player)

    async def on_player_stop(self, **_):
        pass

    async def on_player_finished_playing(self, player, entry, **_):
        await self.update_now_playing(entry, player, is_stopped=True)

        # Start the auto-playlist:
        if not player.playlist.entries and not player.current_entry and self.config.auto_playlist:
            while self.autoplaylist:
                random.shuffle(self.autoplaylist)
                song_url = random.choice(self.autoplaylist)

                try:
                    info = await self.downloader.extract_info(player.playlist.loop, song_url, download=False,
                                                              process=False)
                except downloader.youtube_dl.utils.DownloadError as e:
                    if 'YouTube said:' in e.args[0]:
                        # url is bork, remove from list and put in removed list
                        log.error("Error processing youtube url:\n{}".format(e.args[0]))

                    else:
                        # Probably an error from a different extractor, but I've only seen youtube's
                        log.error("Error processing \"{url}\": {ex}".format(url=song_url, ex=e))

                    await self.remove_from_autoplaylist(song_url, ex=e, delete_from_ap=True)
                    continue

                except Exception as e:
                    log.error("Error processing \"{url}\": {ex}".format(url=song_url, ex=e))

                    self.autoplaylist.remove(song_url)
                    continue

                if info.get('entries', None):  # or .get('_type', '') == 'playlist'
                    log.debug("Playlist found but is unsupported at this time, skipping.")
                    # TODO: Playlist expansion

                try:
                    await player.playlist.add_entry(song_url, channel=None, author=None)
                except exceptions.ExtractionError as e:
                    log.error("Error adding song from autoplaylist: {}".format(e))
                    log.debug('', exc_info=True)
                    continue

                break

            if not self.autoplaylist:
                log.warning("No playable songs in the auto-playlist, disabling.")
                self.config.auto_playlist = False

        else:  # Don't serialize for auto-playlist events
            await self.serialize_queue(player.voice_client.channel.server)

    async def on_player_entry_added(self, player, playlist, entry, **_):
        if entry.meta.get('author') and entry.meta.get('channel'):
            await self.serialize_queue(player.voice_client.channel.server)

    async def on_player_error(self, player, entry, ex, **_):
        if 'channel' in entry.meta:
            await self.safe_send_message(
                entry.meta['channel'],
                "```\nError from FFmpeg:\n{}\n```".format(ex)
            )
        else:
            log.exception("Player error", exc_info=ex)

        async def remove_from_autoplaylist(self, song_url: str, *, ex: Exception = None, delete_from_ap=False):
            if song_url not in self.autoplaylist:
                log.debug("URL \"{}\" not in auto-playlist, ignoring".format(song_url))
                return

            self.autoplaylist.remove(song_url)
            log.info("Removing unplayable song from auto-playlist: %s" % song_url)

            with open(self.config.auto_playlist_removed_file, 'a', encoding='utf8') as f:
                f.write(
                    '# Entry removed {ctime}\n'
                    '# Reason: {ex}\n'
                    '{url}\n\n{sep}\n\n'.format(
                        ctime=time.ctime(),
                        ex=str(ex).replace('\n', '\n#' + ' ' * 10),
                        url=song_url,
                        sep='#' * 32
                    ))

            if delete_from_ap:
                log.info("Updating auto-playlist")
                write_file(self.config.auto_playlist_file, self.autoplaylist)
    # endregion

    # region Queue saving and reloading (Serialization/Deserializion)
    async def serialize_queue(self, server, *, dir=None):
        """
        Serialize the current queue for a server's player to json.
        """

        player = self.get_player_in(server)
        if not player:
            return

        if dir is None:
            dir = 'data/%s/queue.json' % server.id

        async with self.aiolocks['queue_serialization' + ':' + server.id]:
            log.debug("Serializing queue for %s", server.id)

            with open(dir, 'w', encoding='utf8') as f:
                f.write(player.serialize(sort_keys=True))

    async def serialize_all_queues(self, *, dir=None):
        coros = [self.serialize_queue(s, dir=dir) for s in self.servers]
        await asyncio.gather(*coros, return_exceptions=True)

    async def deserialize_queue(self, server, voice_client, playlist=None, *, dir=None) -> MusicPlayer:
        """
        Deserialize a saved queue for a server into a MusicPlayer.  If no queue is saved, returns None.
        """

        if playlist is None:
            playlist = Playlist(self)

        if dir is None:
            dir = 'data/%s/queue.json' % server.id

        async with self.aiolocks['queue_serialization' + ':' + server.id]:
            if not os.path.isfile(dir):
                return None

            log.debug("Deserializing queue for %s", server.id)

            with open(dir, 'r', encoding='utf8') as f:
                data = f.read()

        return MusicPlayer.from_json(data, self, voice_client, playlist)
    # endregion

    # region Sanity Check Helper Functions
    async def _on_ready_sanity_checks(self):
        await self.ensure_appinfo()

        await self._sanity_check_env()
        await self._sanity_check_configs()

    async def _sanity_check_env(self):
        log.debug("Ensuring data folders exist")
        for server in self.servers:
            pathlib.Path('data/%s/' % server.id).mkdir(exist_ok=True)

        with open('data/server_names.txt', 'w', encoding='utf8') as f:
            for server in sorted(self.servers, key=lambda s: int(s.id)):
                f.write('{:<22} {}\n'.format(server.id, server.name))

        if not self.config.save_videos and os.path.isdir(AUDIO_CACHE_PATH):
            if self._delete_old_audio_cache():
                log.debug("Deleted old audio cache")
            else:
                log.debug("Could not delete old audio cache, moving on.")

    async def _sanity_check_configs(self):
        log.debug("Validating config")
        await self.config.async_validate(self)

        log.debug("Validating permissions config")
        await self.permissions.async_validate(self)
    # endregion

    # region Generic Discord API Wrappers
    async def _manual_delete_check(self, message, *, quiet=False):
        if self.config.delete_invoking:
            await self.safe_delete_message(message, quiet=quiet)

    async def safe_send_message(self, dest, content, **kwargs):
        """
        :param delete_delay_multiplicator:
        :suppress_delete:
        """
        delete_delay_multiplicator = kwargs.pop('delete_delay_multiplicator', 1)
        suppress_delete = kwargs.pop('suppress_delete', False)

        msg = await super(MusicBotClient, self).safe_send_message(dest, content, **kwargs)

        if suppress_delete:
            return msg

        if msg and self.config.delete_messages:
            asyncio.ensure_future(
                self.wait_delete_msg(msg, self.config.delete_delay * delete_delay_multiplicator)
            )
            msg = None

        return msg
    # endregion

    # region General Program Lifecycle
    async def restart(self):
        self.exit_signal = exceptions.RestartSignal()
        await self.logout()

    def restart_threadsafe(self):
        asyncio.run_coroutine_threadsafe(self.restart(), self.loop)

    def _cleanup(self):
        try:
            self.loop.run_until_complete(self.logout())
        except:
            pass

        pending = asyncio.Task.all_tasks()
        gathered = asyncio.gather(*pending)

        try:
            gathered.cancel()
            self.loop.run_until_complete(gathered)
            gathered.exception()
        except:
            pass

    # noinspection PyMethodOverriding
    def run(self):
        try:
            self.loop.run_until_complete(self.start(*self.config.auth))

        except discord.errors.LoginFailure:
            # Add if token, else
            raise exceptions.HelpfulError(
                "Bot cannot login, bad credentials.",
                "Remember that each field should be on their own line."
            )

        finally:
            try:
                self._cleanup()
            except Exception:
                log.error("Error in cleanup", exc_info=True)

            self.loop.close()
            if self.exit_signal:
                raise self.exit_signal

    async def logout(self):
        await self.disconnect_all_voice_clients()
        return await super().logout()

    async def on_error(self, event, *args, **kwargs):
        ex_type, ex, stack = sys.exc_info()

        if ex_type == exceptions.HelpfulError:
            log.error("Exception in {}:\n{}".format(event, ex.message))

            await asyncio.sleep(2)  # don't ask
            await self.logout()

        elif issubclass(ex_type, exceptions.Signal):
            self.exit_signal = ex_type
            await self.logout()

        else:
            log.error("Exception in {}".format(event), exc_info=True)

    async def on_resumed(self):
        log.info("\nReconnected to discord.\n")

    async def on_ready(self):
        dlogger = logging.getLogger('discord')
        for h in dlogger.handlers:
            if getattr(h, 'terminator', None) == '':
                dlogger.removeHandler(h)

        log.debug("Connection established, ready to go.")

        self.ws._keep_alive.name = 'Gateway Keepalive'

        if self.init_ok:
            log.debug("Received additional READY event, may have failed to resume")
            return

        await self._on_ready_sanity_checks()
        print()

        log.info('Connected!  Musicbot v{}\n'.format(BOTVERSION))

        self.init_ok = True

        ################################

        log.info("Bot:   {0}/{1}#{2}{3}".format(
            self.user.id,
            self.user.name,
            self.user.discriminator,
            ' [BOT]' if self.user.bot else ' [Userbot]'
        ))

        owner = self.get_owner(voice=True) or self.get_owner()
        if owner and self.servers:
            log.info("Owner: {0}/{1}#{2}\n".format(
                owner.id,
                owner.name,
                owner.discriminator
            ))

            log.info('Server List:')
            [log.info(' - ' + s.name) for s in self.servers]

        elif self.servers:
            log.warning("Owner could not be found on any server (id: %s)\n" % self.config.owner_id)

            log.info('Server List:')
            [log.info(' - ' + s.name) for s in self.servers]

        else:
            log.warning("Owner unknown, bot is not on any servers.")
            if self.user.bot:
                log.warning(
                    "To make the bot join a server, paste this link in your browser. \n"
                    "Note: You should be logged into your main account and have \n"
                    "manage server permissions on the server you want the bot to join.\n"
                    "  " + await self.generate_invite_link()
                )

        print(flush=True)

        if self.config.bound_channels:
            chlist = set(self.get_channel(i) for i in self.config.bound_channels if i)
            chlist.discard(None)

            invalids = set()
            invalids.update(c for c in chlist if c.type == discord.ChannelType.voice)

            chlist.difference_update(invalids)
            self.config.bound_channels.difference_update(invalids)

            if chlist:
                log.info("Bound to text channels:")
                [log.info(' - {}/{}'.format(ch.server.name.strip(), ch.name.strip())) for ch in chlist if ch]
            else:
                print("Not bound to any text channels")

            if invalids and self.config.debug_mode:
                print(flush=True)
                log.info("Not binding to voice channels:")
                [log.info(' - {}/{}'.format(ch.server.name.strip(), ch.name.strip())) for ch in invalids if ch]

            print(flush=True)

        else:
            log.info("Not bound to any text channels")

        if self.config.autojoin_channels:
            chlist = set(self.get_channel(i) for i in self.config.autojoin_channels if i)
            chlist.discard(None)

            invalids = set()
            invalids.update(c for c in chlist if c.type == discord.ChannelType.text)

            chlist.difference_update(invalids)
            self.config.autojoin_channels.difference_update(invalids)

            if chlist:
                log.info("Autojoining voice chanels:")
                [log.info(' - {}/{}'.format(ch.server.name.strip(), ch.name.strip())) for ch in chlist if ch]
            else:
                log.info("Not autojoining any voice channels")

            if invalids and self.config.debug_mode:
                print(flush=True)
                log.info("Cannot autojoin text channels:")
                [log.info(' - {}/{}'.format(ch.server.name.strip(), ch.name.strip())) for ch in invalids if ch]

            autojoin_channels = chlist

        else:
            log.info("Not autojoining any voice channels")
            autojoin_channels = set()

        print(flush=True)
        log.info("Options:")

        log.info("  Command prefix: " + self.config.command_prefix)
        log.info("  Default volume: {}%".format(int(self.config.default_volume * 100)))
        log.info("  Skip threshold: {} votes or {}%".format(
            self.config.skips_required, fixg(self.config.skip_ratio_required * 100)))
        log.info("  Now Playing @mentions: " + ['Disabled', 'Enabled'][self.config.now_playing_mentions])
        log.info("  Auto-Playlist: " + ['Disabled', 'Enabled'][self.config.auto_playlist])
        log.info("  Auto-Pause: " + ['Disabled', 'Enabled'][self.config.auto_pause])
        log.info("  Delete Messages: " + ['Disabled', 'Enabled'][self.config.delete_messages])
        if self.config.delete_messages:
            log.info("    Delete Invoking: " + ['Disabled', 'Enabled'][self.config.delete_invoking])
        log.info("  Debug Mode: " + ['Disabled', 'Enabled'][self.config.debug_mode])
        log.info("  Downloaded songs will be " + ['deleted', 'saved'][self.config.save_videos])
        print(flush=True)

        await self._join_startup_channels(autojoin_channels)

        if self.config.now_playing_as_topic:
            bound_channels = set(self.get_channel(i) for i in self.config.bound_channels if i)
            await self._reset_bound_channels_topic(bound_channels)

        await self.change_presence(
            game=Game(name="{}help".format(self.config.command_prefix))
        )
    # endregion

    # region Bot Lifecycle
    async def on_message(self, message):
        await self.wait_until_ready()

        # Check if we should care at all about the message:

        message_content = message.content.strip()
        if not message_content.startswith(self.config.command_prefix):
            return

        if message.author == self.user:
            log.warning("Ignoring command from myself ({})".format(message.content))
            return

        if (self.config.bound_channels
            and message.channel.id not in self.config.bound_channels
            and not message.channel.is_private
        ):
            return

        # Prepare Handling:

        log.info("{0.id}/{0!s}: {1}".format(message.author, message_content.replace('\n', '\n... ')))

        command, *args = message_content.split(' ')
        # Uh, doesn't this break prefixes with spaces in them?
        # (It doesn't, config parser already breaks them)
        command = command[len(self.config.command_prefix):].lower().strip()

        # Load Command Handler:
        import MusicBot.commands
        handler = getattr(MusicBot.commands, 'cmd_' + command, None)
        if not handler:
            return

        try:
            argspec = inspect.signature(handler)
            params = argspec.parameters.copy()

            # Check general constraints:

            if message.channel.is_private:
                if not (message.author.id == self.config.owner_id and command == 'joinserver'):
                    await self.send_message(message.channel, 'You cannot use me through private messages.')
                    return

            if message.author.id in self.blacklist and message.author.id != self.config.owner_id:
                raise exceptions.PermissionsError(":no_entry_sign: {0.id}/{0!s} is blacklisted (tried to use {1})".format(
                        message.author, command
                    )
                )

            user_permissions = self.permissions.for_user(message.author)
            if message.author.id != self.config.owner_id:
                if user_permissions.command_whitelist and command not in user_permissions.command_whitelist:
                    raise exceptions.PermissionsError("This command is not enabled for your group ({}).".format(
                            user_permissions.name
                        )
                    )
                elif user_permissions.command_blacklist and command in user_permissions.command_blacklist:
                    raise exceptions.PermissionsError("This command is disabled for your group ({}).".format(
                            user_permissions.name
                        )
                    )

            if user_permissions.ignore_non_voice and command in user_permissions.ignore_non_voice:
                await self.check_ignore_non_voice(message)

            # Prepare Handler Call Arguments:

            handler_kwargs = {}

            if params.pop('bot', None):
                handler_kwargs['bot'] = self

            if params.pop('message', None):
                handler_kwargs['message'] = message
            if params.pop('channel', None):
                handler_kwargs['channel'] = message.channel
            if params.pop('author', None):
                handler_kwargs['author'] = message.author
            if params.pop('server', None):
                handler_kwargs['server'] = message.server

            if params.pop('player', None):
                handler_kwargs['player'] = await self.get_player(message.channel)
            if params.pop('_player', None):
                handler_kwargs['_player'] = self.get_player_in(message.server)
            if params.pop('permissions', None):
                handler_kwargs['permissions'] = user_permissions
            if params.pop('user_mentions', None):
                handler_kwargs['user_mentions'] = list(map(message.server.get_member, message.raw_mentions))
            if params.pop('channel_mentions', None):
                handler_kwargs['channel_mentions'] = list(map(message.server.get_channel, message.raw_channel_mentions))
            if params.pop('voice_channel', None):
                handler_kwargs['voice_channel'] = message.server.me.voice_channel

            if params.pop('leftover_args', None):
                handler_kwargs['leftover_args'] = args

            args_expected = []
            for key, param in list(params.items()):

                # parse (*args) as a list of args
                if param.kind == param.VAR_POSITIONAL:
                    handler_kwargs[key] = args
                    params.pop(key)
                    continue

                # parse (*, args) as args rejoined as a string
                # multiple of these arguments will have the same value
                if param.kind == param.KEYWORD_ONLY and param.default == param.empty:
                    handler_kwargs[key] = ' '.join(args)
                    params.pop(key)
                    continue

                doc_key = '[{}={}]'.format(key, param.default) if param.default is not param.empty else key
                args_expected.append(doc_key)

                # Ignore keyword args with default values when the command had no arguments
                if not args and param.default is not param.empty:
                    params.pop(key)
                    continue

                # Assign given values to positional arguments
                if args:
                    arg_value = args.pop(0)
                    handler_kwargs[key] = arg_value
                    params.pop(key)

            # Invalid usage, return docstring:

            if params:
                docs = getattr(handler, '__doc__', None)
                if not docs:
                    docs = 'Usage: {}{} {}'.format(
                        self.config.command_prefix,
                        command,
                        ' '.join(args_expected)
                    )

                docs = dedent(docs)
                await self.safe_send_message(
                    message.channel,
                    '```\n{}\n```'.format(docs.format(command_prefix=self.config.command_prefix)),
                    delete_delay_multiplicator=3
                )
                return

            # All good to go. Let's do this and tell the user how it went:

            response = await handler(**handler_kwargs)
            if response and isinstance(response, Response):
                content = response.content
                if response.reply:
                    content = '{}, {}'.format(message.author.mention, content)

                await self.safe_send_message(
                    message.channel, content,
                    delete_delay_multiplicator=response.delete_delay_multiplicator
                )

        except (exceptions.CommandError, exceptions.HelpfulError, exceptions.ExtractionError) as e:
            log.error("Error in {0}: {1.__class__.__name__}: {1.message}".format(command, e), exc_info=True)

            await self.safe_send_message(
                message.channel,
                '```\n{}\n```'.format(e.message),
                delete_delay_multiplicator=2
            )

        except exceptions.Signal:
            raise

        except BaseException:
            log.error("Exception in on_message", exc_info=True)

            if self.config.debug_mode:
                await self.safe_send_message(
                    message.channel,
                    '```\n{}\n```'.format(traceback.format_exc()),
                    delete_delay_multiplicator=2
                )

        finally:
            if self.config.delete_invoking:
                await self.safe_delete_message(message, quiet=True)

    async def on_voice_state_update(self, before, after):
        """
        Event called when a user changes their voice state
        """

        if not self.init_ok:
            return  # Ignore stuff before ready

        state = VoiceStateUpdate(before, after)

        if state.broken:
            log.voicedebug("Broken voice state update")
            return

        if state.resuming:
            log.debug("Resumed voice connection to {0.server.name}/{0.name}".format(state.voice_channel))

        if not state.changes:
            log.voicedebug("Empty voice state update, likely a session id change")
            return  # Session id change, pointless event

        ################################

        log.voicedebug("Voice state update for {mem.id}/{mem!s} on {ser.name}/{vch.name} -> {dif}".format(
            mem=state.member,
            ser=state.server,
            vch=state.voice_channel,
            dif=state.changes
        ))

        if not state.is_about_my_voice_channel:
            return  # Irrelevant channel

        if state.joining or state.leaving:
            log.info("{0.id}/{0!s} has {1} {2}/{3}".format(
                state.member,
                'joined' if state.joining else 'left',
                state.server,
                state.my_voice_channel
            ))

        if not self.config.auto_pause:
            return

        autopause_msg = "{state} in {channel.server.name}/{channel.name} {reason}"

        auto_paused = self.server_specific_data[after.server]['auto_paused']
        player = await self.get_player(state.my_voice_channel)

        if state.joining and state.empty() and player.is_playing:
            log.info(autopause_msg.format(
                state="Pausing",
                channel=state.my_voice_channel,
                reason="(joining empty channel)"
            ).strip())

            self.server_specific_data[after.server]['auto_paused'] = True
            player.pause()
            return

        if not state.is_about_me:
            if not state.empty(old_channel=state.leaving):
                if auto_paused and player.is_paused:
                    log.info(autopause_msg.format(
                        state="Unpausing",
                        channel=state.my_voice_channel,
                        reason=""
                    ).strip())

                    self.server_specific_data[after.server]['auto_paused'] = False
                    player.resume()
            else:
                if not auto_paused and player.is_playing:
                    log.info(autopause_msg.format(
                        state="Pausing",
                        channel=state.my_voice_channel,
                        reason="(empty channel)"
                    ).strip())

                    self.server_specific_data[after.server]['auto_paused'] = True
                    player.pause()

    async def on_server_update(self, before: discord.Server, after: discord.Server):
        if before.region != after.region:
            log.warning("Server \"%s\" changed regions: %s -> %s" % (after.name, before.region, after.region))

        await self.reconnect_voice_client(after)

    async def on_server_join(self, server: discord.Server):
        log.info("Bot has been added to a server: {}".format(server.name))

        log.debug("Creating data folder for server %s", server.id)
        pathlib.Path('data/%s/' % server.id).mkdir(exist_ok=True)

    async def on_server_remove(self, server: discord.Server):
        log.info("Bot has been removed from server: {}".format(server.name))
        log.debug('Updated server list:')
        [log.debug(' - ' + s.name) for s in self.servers]

        if server.id in self.players:
            self.players.pop(server.id).kill()

    async def on_server_available(self, server: discord.Server):
        if not self.init_ok:
            return  # Ignore pre-ready events

        log.debug("Server \"{}\" has become available.".format(server.name))

        player = self.get_player_in(server)

        if player and player.is_paused:
            if self.server_specific_data[server]['availability_paused']:
                log.debug("Resuming player in \"{}\" due to availability.".format(server.name))
                self.server_specific_data[server]['availability_paused'] = False
                player.resume()

    async def on_server_unavailable(self, server: discord.Server):
        log.debug("Server \"{}\" has become unavailable.".format(server.name))

        player = self.get_player_in(server)

        if player and player.is_playing:
            log.debug("Pausing player in \"{}\" due to unavailability.".format(server.name))
            self.server_specific_data[server]['availability_paused'] = True
            player.pause()
    #endregion
    ""
