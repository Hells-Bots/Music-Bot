import inspect
import json
import pydoc

from discord.ext.commands.bot import _get_variable


class Serializer(json.JSONEncoder):
    def default(self, o):
        if hasattr(o, '__json__'):
            return o.__json__()

        return super().default(o)

    @classmethod
    def deserialize(cls, data):
        if all(x in data for x in Serializable._class_signature):
            factory = pydoc.locate(data['__module__'] + '.' + data['__class__'])
            if factory and issubclass(factory, Serializable):
                return factory._deserialize(data['data'], **cls._get_vars(factory._deserialize))

        return data

    @classmethod
    def _get_vars(cls, func):
        params = inspect.signature(func).parameters.copy()
        args = {}

        for name, param in params.items():
            if param.kind is param.POSITIONAL_OR_KEYWORD and param.default is None:
                args[name] = _get_variable(name)

        return args


class Serializable:
    _class_signature = ('__class__', '__module__', 'data')

    def _enclose_json(self, data):
        return {
            '__class__': self.__class__.__qualname__,
            '__module__': self.__module__,
            'data': data
        }

    # Perhaps convert this into some sort of decorator
    @staticmethod
    def _bad(arg):
        raise TypeError('Argument "%s" must not be None' % arg)

    def serialize(self, *, cls=Serializer, **kwargs):
        return json.dumps(self, cls=cls, **kwargs)

    def __json__(self):
        raise NotImplementedError

    @classmethod
    def _deserialize(cls, raw_json, **kwargs):
        raise NotImplementedError
