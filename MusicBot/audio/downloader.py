import os
import asyncio
import logging
import functools
import youtube_dl

from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

log = logging.getLogger(__name__)


ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'
}

# Redact ytdl's bug-report message, as it's content (i.e. links to their help) is not relevant for us
#  or helpful for users
youtube_dl.utils.bug_reports_message = lambda: ''


class Downloader:
    def __init__(self, download_folder=None):
        self.download_folder = download_folder

        '''
            Alright, here's the problem.  To catch youtube-dl errors for their useful information, I have to
            catch the exceptions with `ignoreerrors` off.  To not break when ytdl hits a dumb video
            (rental videos, etc), I have to have `ignoreerrors` on.  I can change these whenever, but with async
            that's bad.  So I need multiple ytdl objects.
        '''
        self.unsafe_ytdl = youtube_dl.YoutubeDL(ytdl_format_options)
        self.safe_ytdl = youtube_dl.YoutubeDL(ytdl_format_options)
        self.safe_ytdl.params['ignoreerrors'] = True

        if download_folder:
            otmpl = self.unsafe_ytdl.params['outtmpl']
            self.unsafe_ytdl.params['outtmpl'] = os.path.join(download_folder, otmpl)

            otmpl = self.safe_ytdl.params['outtmpl']
            self.safe_ytdl.params['outtmpl'] = os.path.join(download_folder, otmpl)

    @property
    def ytdl(self):
        return self.safe_ytdl

    async def extract_info(self, loop, *args, on_error=None, retry_on_error=False, **kwargs):
        """
            Runs ytdl.extract_info within the threadpool. Returns a future that will fire when it's done.
            If `on_error` is passed and an exception is raised, the exception will be caught and passed to
            on_error as an argument.
        """
        if callable(on_error):
            try:
                return await self._extract_info_executor(loop, self.unsafe_ytdl, *args, **kwargs)
            except Exception as e:
                if asyncio.iscoroutinefunction(on_error):
                    asyncio.ensure_future(on_error(e), loop=loop)
                elif asyncio.iscoroutine(on_error):
                    asyncio.ensure_future(on_error, loop=loop)
                else:
                    loop.call_soon_threadsafe(on_error, e)

                if retry_on_error:
                    return await self._extract_info_executor(loop, self.safe_ytdl, *args, **kwargs)
        else:
            return await self._extract_info_executor(loop, self.unsafe_ytdl, *args, **kwargs)

    @staticmethod
    async def _extract_info_executor(loop, ytdl, *args, **kwargs):
        result = await loop.run_in_executor(
            ThreadPoolExecutor(max_workers=1),
            functools.partial(ytdl.extract_info, *args, **kwargs)
        )
        return result
