from textwrap import dedent

from MusicBot import MusicBotClient
from MusicBot.common_types.responses import Response


async def cmd_help(bot: MusicBotClient, message, command=None):
    """
    Usage:
        {command_prefix}help [command]

    Prints a help message.
    If a command is specified, it prints a help message for that command.
    Otherwise, it lists the available commands.
    """
    if command and bot.user.id not in message.raw_mentions:
        import MusicBot.commands
        cmd = getattr(MusicBot.commands, 'cmd_' + command, None)
        if cmd and not hasattr(cmd, 'dev_cmd'):
            return Response(
                "```\n{}```".format(
                    dedent(cmd.__doc__)
                ).format(command_prefix=bot.config.command_prefix),
                delete_delay_multiplicator=5
            )
        else:
            return Response("No such command")

    else:
        if bot.user.id not in message.raw_mentions:
            return

        helpmsg = "**Available commands**\n```"
        commands = []

        import MusicBot.commands
        for attribute in dir(MusicBot.commands):
            if (attribute.startswith('cmd_')
                and attribute != 'cmd_help'
            ):
                command_name = attribute.replace('cmd_', '').lower()
                commands.append("{}{}".format(bot.config.command_prefix, command_name))

        helpmsg += ", ".join(commands)
        helpmsg += "```\nYou can also use `{}help x` for more info about each command.".format(
            bot.config.command_prefix
        )

        return Response(helpmsg, reply=True, delete_delay_multiplicator=3)

async def cmd_perms(bot: MusicBotClient, author, channel, server, permissions):
    """
    Usage:
        {command_prefix}perms

    Sends the user a list of their own permissions.
    """

    lines = ['Command permissions in %s\n' % server.name, '```', '```']

    for perm in permissions.__dict__:
        if perm in ['user_list'] or permissions.__dict__[perm] == set():
            continue

        lines.insert(len(lines) - 1, "%s: %s" % (perm, permissions.__dict__[perm]))

    await bot.safe_send_message(author, '\n'.join(lines), delete_delay_multiplicator=5)
    return Response("\N{OPEN MAILBOX WITH RAISED FLAG}")
