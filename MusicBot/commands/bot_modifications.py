import discord
from aiohttp import Timeout

from MusicBot import MusicBotClient
from MusicBot.common_types import exceptions
from MusicBot.common_types.responses import Response


async def cmd_setavatar(bot: MusicBotClient, message, url=None):
    bot.ensure_owner_only()

    """
    Usage:
        {command_prefix}setavatar [url]

    Changes the bot's avatar.
    Attaching a file and leaving the url parameter blank also works.
    """

    if message.attachments:
        thing = message.attachments[0]['url']
    else:
        thing = url.strip('<>')

    try:
        with Timeout(10):
            async with bot.aiosession.get(thing) as res:
                await bot.edit_profile(avatar=await res.read())

    except Exception as e:
        raise exceptions.CommandError("Unable to change avatar: {}".format(e))

    return Response("\N{OK HAND SIGN}")

async def cmd_setname(bot: MusicBotClient, leftover_args, name):
    """
    Usage:
        {command_prefix}setname name

    Changes the bot's username.
    Note: This operation is limited by discord to twice per hour.
    """

    name = ' '.join([name, *leftover_args])

    try:
        await bot.edit_profile(username=name)

    except discord.HTTPException:
        raise exceptions.CommandError(
            "Failed to change name.  Did you change names too many times?  "
            "Remember name changes are limited to twice per hour.")

    except Exception as e:
        raise exceptions.CommandError(e)

    return Response("\N{OK HAND SIGN}")

async def cmd_setnick(bot: MusicBotClient, server, channel, leftover_args, nick):
    """
    Usage:
        {command_prefix}setnick nick

    Changes the bot's nickname.
    """

    if not channel.permissions_for(server.me).change_nickname:
        raise exceptions.CommandError("Unable to change nickname: no permission.")

    nick = ' '.join([nick, *leftover_args])

    try:
        await bot.change_nickname(server.me, nick)
    except Exception as e:
        raise exceptions.CommandError(e)

    return Response("\N{OK HAND SIGN}")
