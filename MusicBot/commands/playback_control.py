from MusicBot import MusicBotClient
from MusicBot.audio.player import MusicPlayer
from MusicBot.common_types import exceptions
from MusicBot.common_types.responses import Response


async def cmd_pause(bot: MusicBotClient, player):
    """
    Usage:
        {command_prefix}pause

    Pauses playback of the current song.
    """

    if player.is_playing:
        player.pause()

    else:
        raise exceptions.CommandError('Player is not playing.')


async def cmd_resume(bot: MusicBotClient, player):
    """
    Usage:
        {command_prefix}resume

    Resumes playback of a paused song.
    """

    if player.is_paused:
        player.resume()

    else:
        raise exceptions.CommandError('Player is not paused.')


async def cmd_stop(bot: MusicBotClient, player: MusicPlayer, author):
    """
    Usage:
        {command_prefix}stop

    Stops all music, i.e. ends the current track and clears the queue.
    """

    if player.is_stopped:
        raise exceptions.CommandError("Already stopped!")

    player.skip()
    player.playlist.clear()
    return Response(
        ':put_litter_in_its_place: :zzz:'
    )


async def cmd_volume(bot: MusicBotClient, message, player, new_volume=None):
    """
    Usage:
        {command_prefix}volume (+/-)[volume]

    Sets the playback volume. Accepted values are from 1 to 100.
    Putting + or - before the volume will make the volume change relative to the current volume.
    """

    if not new_volume:
        return Response(
            'Current volume: `%s%%`' % int(player.volume * 100), reply=True
        )

    relative = False
    if new_volume[0] in '+-':
        relative = True

    try:
        new_volume = int(new_volume)

    except ValueError:
        raise exceptions.CommandError('{} is not a valid number'.format(new_volume))

    vol_change = None
    if relative:
        vol_change = new_volume
        new_volume += (player.volume * 100)

    old_volume = int(player.volume * 100)

    if 0 < new_volume <= 100:
        player.volume = new_volume / 100.0

        return Response(
            'Changed volume from %d to %d' % (old_volume, new_volume), reply=True
        )

    else:
        if relative:
            raise exceptions.CommandError(
                'Unreasonable volume change provided: {}{:+} -> {}%.  Provide a change between {} and {:+}.'.format(
                    old_volume, vol_change, old_volume + vol_change, 1 - old_volume, 100 - old_volume)
            )
        else:
            raise exceptions.CommandError(
                'Unreasonable volume provided: {}%. Provide a value between 1 and 100.'.format(new_volume)
            )
