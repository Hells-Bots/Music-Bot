from datetime import timedelta

from MusicBot import MusicBotClient
from MusicBot.audio.player import MusicPlayer
from MusicBot.common_types.responses import Response
from MusicBot.helpers.constants import DISCORD_MSG_CHAR_LIMIT


async def cmd_queue(bot: MusicBotClient, channel, player):
    """
    Usage:
        {command_prefix}queue

    Prints the current song queue.
    """

    lines = []
    unlisted = 0
    andmoretext_approximate_length = len('* ... and %s more*' % len(player.playlist.entries))

    if player.current_entry:
        song_progress = str(timedelta(seconds=player.progress))
        song_total = str(timedelta(seconds=player.current_entry.duration))
        prog_str = '`[%s/%s]`' % (song_progress, song_total)

        if player.current_entry.meta.get('channel', False) and player.current_entry.meta.get('author', False):
            lines.append("Currently Playing: **%s**, added by %s %s\n" % (
                player.current_entry.title, player.current_entry.meta['author'].display_name, prog_str))
        else:
            lines.append("Now Playing: **%s** %s\n" % (player.current_entry.title, prog_str))

    for i, item in enumerate(player.playlist, 1):
        if item.meta.get('channel', False) and item.meta.get('author', False):
            nextline = '`{}.` **{}** added by {}'.format(
                i, item.title, item.meta['author'].display_name
            )
        else:
            nextline = '`{}.` **{}**'.format(
                i, item.title
            )

        currentlinesum = sum(len(x) + 1 for x in lines)  # +1 is for newline char

        if currentlinesum + len(nextline) + andmoretext_approximate_length > DISCORD_MSG_CHAR_LIMIT:
            if currentlinesum + andmoretext_approximate_length:
                unlisted += 1
                continue

        lines.append(nextline)

    if unlisted:
        lines.append('\n*... and %s more*' % unlisted)

    if not lines:
        lines.append(
            'There are no songs queued! Queue something with {}play.'.format(bot.config.command_prefix))

    message = '\n'.join(lines)
    return Response(
        message
    )

async def cmd_np(bot: MusicBotClient, player: MusicPlayer):
    """
    Usage:
        {command_prefix}np

    Displays the current song in chat.
    """

    if player.current_entry:
        song_progress = str(timedelta(seconds=player.progress))
        song_total = str(timedelta(seconds=player.current_entry.duration))
        prog_str = ('`[{progress}/{total}]`').format(
            progress=song_progress, total=song_total
        )

        extra_text = ", Progress: {progress}\n{emoji} <{url}>".format(
            progress=prog_str,
            emoji='\N{WHITE RIGHT POINTING BACKHAND INDEX}',
            url=player.current_entry.url
        )

        await bot.update_now_playing(player.current_entry, player, player.is_paused, player.is_stopped, True, extra_text)
    else:
        return Response(
            'There are no songs queued. Maybe queue something with {}play?'.format(bot.config.command_prefix)
        )
