import datetime
import logging

import asyncio

import discord

from MusicBot import MusicBotClient
from MusicBot.common_types.exceptions import CommandError
from MusicBot.common_types.responses import Response
from MusicBot.helpers.file_handling import write_file


log = logging.getLogger(MusicBotClient.__name__)


async def cmd_blacklist(bot: MusicBotClient, message, user_mentions, option, something):
    """
    Usage:
        {command_prefix}blacklist [ + | - | add | remove ] @UserName [@UserName2 ...]

    Add or remove users to the blacklist.
    Blacklisted users are forbidden from using bot commands.
    """

    if not user_mentions:
        raise CommandError("No users listed.")

    if option not in ['+', '-', 'add', 'remove']:
        raise CommandError(
            'Invalid option "%s" specified, use +, -, add, or remove' % option
        )

    for user in user_mentions.copy():
        if user.id == bot.config.owner_id:
            print("[Commands:Blacklist] The owner cannot be blacklisted.")
            user_mentions.remove(user)

    old_len = len(bot.blacklist)

    if option in ['+', 'add']:
        bot.blacklist.update(user.id for user in user_mentions)

        write_file(bot.config.blacklist_file, bot.blacklist)

        return Response(
            '%s users have been added to the blacklist.' % (len(bot.blacklist) - old_len), reply=True
        )

    else:
        if bot.blacklist.isdisjoint(user.id for user in user_mentions):
            return Response(
                'None of those users are in the blacklist.', reply=True
            )

        else:
            bot.blacklist.difference_update(user.id for user in user_mentions)
            write_file(bot.config.blacklist_file, bot.blacklist)

            return Response(
                '%s users have been removed from the blacklist.' % (old_len - len(bot.blacklist)), reply=True
            )

async def cmd_joinserver(bot: MusicBotClient, message, server_link=None):
    """
    Usage:
        {command_prefix}joinserver invite_link

    Asks the bot to join a server.  Note: Bot accounts cannot use invite links.
    """

    bot.ensure_owner_only()

    if bot.user.bot:
        url = await bot.generate_invite_link()
        return Response(
            "Bot accounts can't use invite links! Go here to add me to a server: \n{}".format(url), reply=True
        )

    try:
        if server_link:
            await bot.accept_invite(server_link)
            return Response("\N{THUMBS UP SIGN}")

    except:
        raise CommandError('Invalid URL provided:\n{}\n'.format(server_link))

async def cmd_summon(bot: MusicBotClient, channel, server, author, voice_channel):
    """
    Usage:
        {command_prefix}summon

    Call the bot to the summoner's voice channel.
    """

    if not author.voice_channel:
        raise CommandError('You are not in a voice channel!')

    voice_client = bot.voice_client_in(server)
    if voice_client and server == author.voice_channel.server:
        await voice_client.move_to(author.voice_channel)
        return

    chperms = author.voice_channel.permissions_for(server.me)

    if not chperms.connect:
        log.warning("Cannot join channel \"{}\", no permission.".format(author.voice_channel.name))
        return Response(
            "```Cannot join channel \"{}\", no permission.```".format(author.voice_channel.name)
        )

    elif not chperms.speak:
        log.warning("Will not join channel \"{}\", no permission to speak.".format(author.voice_channel.name))
        return Response(
            "```Will not join channel \"{}\", no permission to speak.```".format(author.voice_channel.name)
        )

    log.info("Joining {0.server.name}/{0.name}".format(author.voice_channel))

    player = await bot.get_player(author.voice_channel, create=True, deserialize=bot.config.persistent_queue)

    if player.is_stopped:
        player.play()
