import asyncio
import logging
import random
import shlex

import time
import traceback

from discord import Channel

from MusicBot import MusicBotClient
from MusicBot.audio.player import MusicPlayer
from MusicBot.common_types import exceptions
from MusicBot.common_types.responses import Response
from MusicBot.helpers.formaters import fixg

log = logging.getLogger(MusicBotClient.__name__)


async def _send_shuffle_animation(bot: MusicBotClient, channel: Channel):
    cards = ['\N{BLACK SPADE SUIT}', '\N{BLACK CLUB SUIT}', '\N{BLACK HEART SUIT}', '\N{BLACK DIAMOND SUIT}']

    hand = await bot.send_message(channel, '...')
    for x in range(4):
        random.shuffle(cards)
        await bot.safe_edit_message(hand, ' '.join(cards))
        await asyncio.sleep(0.42)

    await bot.safe_delete_message(hand, quiet=True)


async def cmd_play(bot: MusicBotClient, player: MusicPlayer, channel, author, permissions, leftover_args, song_url):
    """
    Usage:
        {command_prefix}play <song_link>
        {command_prefix}play <text to search for>
        {command_prefix}play <modifier> <song_link>
        {command_prefix}play <modifier> <text to search for>

    Adds the song to the queue.  If a link is not provided, the first
    result from a youtube search is added to the queue.
    Available modifiers: `-loop`, `-shuffle`
    """

    if permissions.max_songs and player.playlist.count_for_user(author) >= permissions.max_songs:
        raise exceptions.PermissionsError("You have reached your enqueued song limit (%s)" % permissions.max_songs)

    await bot.send_typing(channel)

    detected_modifiers = {}
    available_modifiers = {
        'l':        lambda: {'do_loop': True},
        'loop':     lambda: {'do_loop': True},
        's':        lambda: {'do_shuffle': True},
        'shuffle':  lambda: {'do_shuffle': True},
    }

    for k, v in available_modifiers.items():
        k = bot.config.command_prefix + k
        if song_url == k:
            song_url = ' '.join(leftover_args)
            detected_modifiers = v()
            break  # only one modifier allowed
    else:
        if leftover_args:
            song_url = ' '.join([song_url, *leftover_args])

    if not (song_url.startswith('http://') or song_url.startswith('https://')):
        song_url = song_url.replace(':', '-')  # fix for ytdl extractors assuming stuff like "aha: " is a protocol

    try:
        info = await bot.downloader.extract_info(
            player.playlist.loop,
            song_url,
            download=False,
            process=False
        )
    except Exception as e:
        raise exceptions.CommandError(e)

    if (
        not info
        or info.get('is_live', False)
    ):
        raise exceptions.CommandError("The video %s cannot be played." % song_url)

    # Abstract the search handling away from the user
    # Our ytdl options allow us to use search strings as input urls
    if info.get('url', '').startswith('ytsearch'):
        info = await bot.downloader.extract_info(
            player.playlist.loop,
            song_url,
            download=False,
            process=True,  # ASYNC LAMBDAS WHEN
            on_error=lambda err: asyncio.ensure_future(
                bot.safe_send_message(
                    channel,
                    "```\n%s\n```" % err,
                    delete_delay_multiplicator=3
                ), loop=bot.loop),
            retry_on_error=True
        )

        if not info:
            raise exceptions.CommandError(
                "Error extracting info from search string, youtubedl returned no data. " +
                "You may need to restart the bot if this continues to happen."
            )

        if not all(info.get('entries', [])):
            log.debug("Got empty list, no data")
            return

        # TODO: handle 'webpage_url' being 'ytsearch:...' or extractor type
        song_url = info['entries'][0]['webpage_url']
        info = await bot.downloader.extract_info(player.playlist.loop, song_url, download=False, process=False)

    if 'entries' in info:
        # Handle Playlist

        # We have to do extra checks anyways because you can request an arbitrary number of search results
        if not permissions.allow_playlists and ':search' in info['extractor'] and len(info['entries']) > 1:
            raise exceptions.PermissionsError("You are not allowed to request playlists")

        if detected_modifiers.get('do_loop'):
            raise exceptions.HelpfulError(
                'Looping is not supported for playlists.',
                'Please just use the `play` command.'
            )

        info['entries'] = list(info['entries'])
        num_songs = len(info['entries'])

        if permissions.max_playlist_length and num_songs > permissions.max_playlist_length:
            raise exceptions.PermissionsError(
                "Playlist has too many entries (%s > %s)" % (
                    num_songs, permissions.max_playlist_length
                )
            )
        if permissions.max_songs and player.playlist.count_for_user(author) + num_songs > permissions.max_songs:
            raise exceptions.PermissionsError(
                "Playlist entries + your already queued songs reached limit (%s + %s > %s)" % (
                    num_songs, player.playlist.count_for_user(author), permissions.max_songs
                )
            )

        t0 = time.time()

        if detected_modifiers.get('do_shuffle'):
            random.shuffle(info['entries'])
            await _send_shuffle_animation(bot, channel)

        busymsg = await bot.safe_send_message(
            channel,
            "Processing %s songs from Playlist **%s**..." % (num_songs, info.get('title', '')),
            suppress_delete=True
        )
        await bot.send_typing(channel)

        extractor = info['extractor'].lower()
        if extractor == 'youtube:playlist':
            process_playlist_method = player.playlist.async_process_youtube_playlist
        elif extractor in ['soundcloud:set', 'soundcloud:user', 'bandcamp:album']:
            process_playlist_method = player.playlist.async_process_sc_bc_playlist
        else:
            raise exceptions.CommandError('Playlist %s is not supported.' % song_url)

        try:
            entries_added = await process_playlist_method(
                info,
                channel=channel,
                author=author
            )
        except Exception:
            log.error("Error processing playlist", exc_info=True)
            raise exceptions.CommandError('Error while handling playlist %s.' % song_url)

        songs_processed = len(entries_added)
        drop_count = 0
        skipped = False

        if permissions.max_song_length:
            for e in entries_added.copy():
                if e.duration > permissions.max_song_length:
                    try:
                        player.playlist.entries.remove(e)
                        entries_added.remove(e)
                        drop_count += 1
                    except:
                        pass

            if drop_count:
                log.debug("Dropped %s songs" % drop_count)

            if player.current_entry and player.current_entry.duration > permissions.max_song_length:
                await bot.safe_delete_message(bot.server_specific_data[channel.server]['last_np_msg'])
                bot.server_specific_data[channel.server]['last_np_msg'] = None
                skipped = True
                player.skip()
                entries_added.pop()

        await bot.safe_delete_message(busymsg)

        songs_added = len(entries_added)
        ttime = time.time() - t0

        log.info("Processed {}/{} songs in {} seconds".format(
            songs_processed, num_songs, fixg(ttime)
        ))

        if not songs_added:
            text = "No songs were added because of errors during processing."
            if skipped:
                text += "\nAdditionally, the current song was skipped for being too long."
            raise exceptions.CommandError(text)

        return Response(
            "Enqueued {} songs to be played in {} seconds".format(
                songs_added, fixg(ttime, 1)
            )
        )

    else:
        # Handle single entry

        if detected_modifiers.get('do_shuffle'):
            raise exceptions.HelpfulError(
                'Shuffle is not supported for a single entry.',
                'Please just use the `play` command.'
            )

        if (
            permissions.max_song_length
            and permissions.max_song_length > 0
            and (info.get('duration', 0) > permissions.max_song_length or detected_modifiers.get('do_loop'))
        ):
            raise exceptions.PermissionsError("Song duration exceeds limit (%s > %s)" % (
                (info['duration'] if not detected_modifiers.get('do_loop') else 'Infinity'), permissions.max_song_length
            )
                                              )

        try:
            entry, position = await player.playlist.add_entry(
                song_url,
                channel=channel,
                author=author,
                do_loop=detected_modifiers.get('do_loop')
            )

        except exceptions.WrongEntryTypeError as e:
            if e.use_url == song_url:
                log.warning("Determined incorrect entry type, but suggested url is the same.")

            log.debug("Assumed url \"%s\" was a single entry, was actually a playlist" % song_url)
            log.debug("Using \"%s\" instead" % e.use_url)

            return await cmd_play(bot, player, channel, author, permissions, leftover_args, e.use_url)

        reply_text = "Enqueued **%s** to be played for %s. Position in queue: %s"
        btext = entry.title.replace('*', '')

    if position == 1 and player.is_stopped:
        position = 'Up next!'
    elif player.current_entry.meta.get('do_loop', False):
        reply_text += ', but we will wait until the current loop is skipped.'
    else:
        try:
            time_until = await player.playlist.estimate_time_until(position, player)
            reply_text += ' - estimated time until playing: ' + str(time_until)
        except:
            traceback.print_exc()

    reply_text %= (btext, author.display_name, position)
    return Response(reply_text, delete_delay_multiplicator=2)


async def cmd_search(bot: MusicBotClient, player, channel, author, permissions, leftover_args):
    """
    Usage:
        {command_prefix}search [service] query

    Searches a service for a video and adds it to the queue.
    - service: any one of the following services:
        - youtube (yt) (default if unspecified)
        - soundcloud (sc)
    """

    if permissions.max_songs and player.playlist.count_for_user(author) > permissions.max_songs:
        raise exceptions.PermissionsError("You have reached your playlist item limit (%s)" % permissions.max_songs)

    def argcheck():
        if not leftover_args:
            # noinspection PyUnresolvedReferences
            raise exceptions.CommandError("Please specify a search query.\n%s" % (
                dedent(self.cmd_search.__doc__.format(command_prefix=self.config.command_prefix))
            )
                                          )

    argcheck()

    try:
        leftover_args = shlex.split(' '.join(leftover_args))
    except ValueError:
        raise exceptions.CommandError("Please quote your search query properly.")

    service = 'youtube'
    items_requested = 5
    services = {
        'youtube': 'ytsearch',
        'soundcloud': 'scsearch',
        'yt': 'ytsearch',
        'sc': 'scsearch'
    }

    if leftover_args[0] in services:
        service = leftover_args.pop(0)
        argcheck()

    if leftover_args[0][0] in '\'"':
        lchar = leftover_args[0][0]
        leftover_args[0] = leftover_args[0].lstrip(lchar)
        leftover_args[-1] = leftover_args[-1].rstrip(lchar)

    search_query = '%s%s:%s' % (services[service], items_requested, ' '.join(leftover_args))

    search_msg = await bot.send_message(channel, "Searching for videos...")
    await bot.send_typing(channel)

    try:
        info = await bot.downloader.extract_info(player.playlist.loop, search_query, download=False, process=True)
    except Exception as result_entry:
        await bot.safe_edit_message(search_msg, str(result_entry), send_if_fail=True)
        return
    else:
        await bot.safe_delete_message(search_msg)

    if not info:
        return Response("No videos found. Maybe try using less keywords.")

    def check(m):
        return (
                m.content.lower()[0] in 'yn' or
                m.content.lower().startswith('exit') or
                m.content.startswith(bot.config.command_prefix)
        )

    for result_entry in info['entries']:
        result_message = await bot.safe_send_message(
            channel,
            "Result %s/%s: %s" % (
                info['entries'].index(result_entry) + 1, len(info['entries']), result_entry['webpage_url']
            ),
            suppress_delete=True
        )

        confirm_message = await bot.safe_send_message(channel, "Is this ok? Send `y`, `n` or `exit`",
                                                      suppress_delete=True)
        response_message = await bot.wait_for_message(30, author=author, channel=channel, check=check)

        if not response_message:
            # = timed out without response
            await bot.safe_delete_message(result_message)
            await bot.safe_delete_message(confirm_message)
            return Response("okay, nevermind.")

        if response_message.content.lower().startswith('y'):
            await bot.safe_delete_message(result_message)
            await bot.safe_delete_message(confirm_message)
            await bot.safe_delete_message(response_message)

            import MusicBot.commands
            await MusicBot.commands.cmd_play(bot, player, channel, author, permissions, [], result_entry['webpage_url'])

            return Response("Alright, let's do this.")

        else:
            await bot.safe_delete_message(result_message)
            await bot.safe_delete_message(confirm_message)
            await bot.safe_delete_message(response_message)
            if not response_message.content.lower().startswith('n'):
                return

    # Exceeded specified number of results
    return Response(
        'Sorry, maybe try adding more keywords.'
    )


async def cmd_shuffle(bot: MusicBotClient, channel, player):
    """
    Usage:
        {command_prefix}shuffle

    Shuffles the playlist.
    """

    player.playlist.shuffle()

    await _send_shuffle_animation(bot, channel)

    return Response(
        "\N{OK HAND SIGN}"
    )


async def cmd_skip(bot: MusicBotClient, player, channel, author, message, permissions, voice_channel):
    """
    Usage:
        {command_prefix}skip

    Skips the current song when enough votes are cast, or by the bot owner.
    """

    if player.is_stopped:
        raise exceptions.CommandError("Can't skip! The player is not playing!")

    if not player.current_entry:
        if player.playlist.peek():
            if player.playlist.peek()._is_downloading:
                return Response(
                    "The next song (%s) is downloading, please wait." % player.playlist.peek().title
                )

            elif player.playlist.peek().is_downloaded:
                print("\N{OK HAND SIGN}")
            else:
                print("Something odd is happening. "
                      "You might want to restart the bot if it doesn't start working."
                      )
        else:
            print("Something strange is happening. "
                  "You might want to restart the bot if it doesn't start working."
                  )

    if (author.id == bot.config.owner_id
            or permissions.instaskip
            or author == player.current_entry.meta.get('author', None)
    ):
        player.skip()
        return Response(':ok_hand:')

    num_voice = sum(1 for m in voice_channel.voice_members if not (
            m.deaf or m.self_deaf or m.id in [bot.config.owner_id, bot.user.id]))

    num_skips = player.skip_state.add_skipper(author.id, message)

    skips_remaining = min(
        bot.config.skips_required,
        int(num_voice * bot.config.skip_ratio_required)
    ) - num_skips

    if skips_remaining <= 0:
        player.skip()
        return Response(
            'Your skip for **{}** was counted.'
            '\nThe vote to skip has been passed.'.format(
                player.current_entry.title
            ),
            reply=True
        )
    else:
        return Response(
            'Your skip for **{}** was counted.'
            '\n**{}** more {} required to vote to skip this song.'.format(
                player.current_entry.title,
                skips_remaining,
                'person is' if skips_remaining == 1 else 'persons are'
            ),
            reply=True
        )


async def cmd_skipto(bot: MusicBotClient, player, author, message, permissions, leftover_args):
    """
    Usage:
        {command_prefix}skipto position

    Skips directly to a specific position in the queue.
    Only works if the sender is the owner or has instaskip rights.
    """

    if len(leftover_args) != 1:
        raise exceptions.CommandError('You must provide exactly one number!')

    try:
        position = int(leftover_args[0])
    except ValueError:
        raise exceptions.CommandError("The given value `%s` is not a number!" % leftover_args[0])

    if player.is_stopped:
        raise exceptions.CommandError("Can't skip! The player is not playing!")

    if not player.current_entry:
        if player.playlist.peek():
            if player.playlist.peek()._is_downloading:
                return Response(
                    "The next song (%s) is downloading, please wait." % player.playlist.peek().title
                )

            elif player.playlist.peek().is_downloaded:
                print("\N{OK HAND SIGN}")
            else:
                print("Something odd is happening. "
                      "You might want to restart the bot if it doesn't start working.")
        else:
            print("Something strange is happening. "
                  "You might want to restart the bot if it doesn't start working.")

    if author.id == bot.config.owner_id \
            or permissions.instaskip \
            or author == player.current_entry.meta.get('author', None):
        player.playlist.skip_over(position - 1)
        player.skip()
        return Response(':ok_hand:')


async def cmd_move(bot: MusicBotClient, player, author, message, permissions, leftover_args):
    """
    Usage:
        {command_prefix}move old_position new_position

    Moves an entry in the queue to a new position in the queue.
    """

    if len(leftover_args) != 2:
        raise exceptions.CommandError('You must provide exactly two numbers!')

    try:
        old_pos = int(leftover_args[0])
    except ValueError:
        raise exceptions.CommandError("The given value `%s` is not a number!" % leftover_args[0])

    try:
        new_pos = int(leftover_args[1])
    except ValueError:
        raise exceptions.CommandError("The given value `%s` is not a number!" % leftover_args[1])

    playlist_count = player.playlist.count_total()

    if (old_pos > playlist_count
            or old_pos < 1
    ):
        raise exceptions.CommandError('The given old position is out of the bounds of the queue!')

    if (new_pos > playlist_count
            or new_pos < 1
    ):
        raise exceptions.CommandError('The given new position is out of the bounds of the queue!')

    player.playlist.move(old_pos - 1, new_pos - 1)

    return Response('\N{OK HAND SIGN}')


async def cmd_remove(bot: MusicBotClient, player, author, message, permissions, leftover_args):
    """
    Usage:
        {command_prefix}remove position

    Removes an entry from the queue.
    """

    try:
        position = int(leftover_args[0]) - 1
    except ValueError:
        raise exceptions.CommandError('The given position is not a valid number!')

    if (
        position < 0
        or position >= player.playlist.count_total()
    ):
        raise exceptions.CommandError('The given position is out of the bounds of the queue!')

    entry = player.playlist.entries[position]
    entry_detail_formatted = '**{}**, originally added by {}'.format(
        entry.title, entry.meta['author'].display_name
    )

    player.playlist.remove(position)

    return Response('\N{OK HAND SIGN} Removed %s!' % entry_detail_formatted)


async def cmd_clear(bot: MusicBotClient, player):
    """
    Usage:
        {command_prefix}clear

    Clears the playlist.
    """

    player.playlist.clear()
    return Response('\N{PUT LITTER IN ITS PLACE SYMBOL}')
