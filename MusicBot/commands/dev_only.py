import traceback

import asyncio

from MusicBot import MusicBotClient
from MusicBot.common_types.responses import Response


async def cmd_debug(bot: MusicBotClient, message, _player, *, data):
    bot.ensure_dev_only()

    codeblock = "```py\n{}\n```"
    result = None

    if data.startswith('```') and data.endswith('```'):
        data = '\n'.join(data.rstrip('`\n').split('\n')[1:])

    code = data.strip('` \n')

    try:
        result = eval(code)
    except:
        try:
            exec(code)
        except Exception as e:
            traceback.print_exc(chain=False)
            return Response("{}: {}".format(type(e).__name__, e))

    if asyncio.iscoroutine(result):
        result = await result

    return Response(codeblock.format(result), delete_delay_multiplicator=2)
