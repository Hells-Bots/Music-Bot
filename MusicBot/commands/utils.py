from collections import defaultdict
from io import BytesIO

import discord

from MusicBot import MusicBotClient
from MusicBot.common_types.exceptions import CommandError
from MusicBot.common_types.responses import Response


async def cmd_pldump(bot: MusicBotClient, channel, song_url):
    """
    Usage:
        {command_prefix}pldump url

    Dumps the individual urls of a playlist
    """

    try:
        info = await bot.downloader.extract_info(bot.loop, song_url.strip('<>'), download=False, process=False)
    except Exception as e:
        raise CommandError("Could not extract info from input url\n%s\n" % e)

    if not info:
        raise CommandError("Could not extract info from input url, no data.")

    if not info.get('entries', None):
        if info.get('url', None) != info.get('webpage_url', info.get('url', None)):
            raise CommandError("This does not seem to be a playlist.")
        else:
            return await bot.cmd_pldump(channel, info.get(''))

    linegens = defaultdict(lambda: None, **{
        "youtube": lambda d: 'https://www.youtube.com/watch?v=%s' % d['id'],
        "soundcloud": lambda d: d['url'],
        "bandcamp": lambda d: d['url']
    })

    exfunc = linegens[info['extractor'].split(':')[0]]

    if not exfunc:
        raise CommandError("Could not extract info from input url, unsupported playlist type.")

    with BytesIO() as fcontent:
        for item in info['entries']:
            fcontent.write(exfunc(item).encode('utf8') + b'\n')

        fcontent.seek(0)
        await bot.send_file(channel, fcontent, filename='playlist.txt', content="Here's the url dump for <%s>" % song_url)

    return Response(
        "\N{OPEN MAILBOX WITH RAISED FLAG}"
    )
