from MusicBot import MusicBotClient
from MusicBot.common_types import exceptions
from MusicBot.common_types.responses import Response


async def cmd_disconnect(bot: MusicBotClient, server):
    await bot.disconnect_voice_client(server)
    return Response("\N{DASH SYMBOL}")

async def cmd_restart(bot: MusicBotClient, channel):
    await bot.safe_send_message(channel, "\N{WAVING HAND SIGN}")
    await bot.disconnect_all_voice_clients()
    raise exceptions.RestartSignal()

async def cmd_shutdown(bot: MusicBotClient, channel):
    await bot.safe_send_message(channel, "\N{WAVING HAND SIGN}")
    await bot.disconnect_all_voice_clients()
    raise exceptions.TerminateSignal()
