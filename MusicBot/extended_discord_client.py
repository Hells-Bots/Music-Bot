import logging
import time
import asyncio
import aiohttp

import discord
from discord import ChannelType

from MusicBot.common_types import exceptions
from MusicBot.helpers.constants import DISCORD_MSG_CHAR_LIMIT


log = logging.getLogger(__name__)


class ExtendedDiscordClient(discord.Client):
    """
    Extends the discord.py Client with some Helper Functions
    """

    # region General Helper Functions
    def get_owner(self, *, server=None, voice=False):
        return discord.utils.find(
            lambda m: m.id == self.config.owner_id and (m.voice_channel if voice else True),
            server.members if server else self.get_all_members()
        )
    # endregion

    # region Text Chat Actions
    async def safe_send_message(self, dest, content, **kwargs):
        tts                     = kwargs.pop('tts',         False)
        quiet                   = kwargs.pop('quiet',       False)
        allow_none              = kwargs.pop('allow_none',  True)

        msg = None
        lfunc = log.debug if quiet else log.warning

        try:
            if content is not None or allow_none:
                msg = await self.send_message(dest, content, tts=tts)

        except discord.Forbidden:
            lfunc("Cannot send message to \"%s\", no permission", dest.name)
        except discord.NotFound:
            lfunc("Cannot send message to \"%s\", probably got an invalid channel", dest.name)
        except discord.HTTPException:
            if len(content) > DISCORD_MSG_CHAR_LIMIT:
                lfunc("Message is over the message size limit (%s chars)", DISCORD_MSG_CHAR_LIMIT)
            else:
                lfunc("Failed to send message")
                log.noise("Got HTTPException trying to send message to %s: %s", dest, content)

        return msg

    async def safe_delete_message(self, message, *, quiet=False):
        lfunc = log.debug if quiet else log.warning

        try:
            return await self.delete_message(message)

        except discord.Forbidden:
            lfunc("Cannot delete message \"{}\", no permission".format(message.clean_content))
        except discord.NotFound:
            lfunc("Cannot delete message \"{}\", message not found".format(message.clean_content))

    async def safe_edit_message(self, message, new, *, send_if_fail=False, quiet=False):
        lfunc = log.debug if quiet else log.warning

        try:
            return await self.edit_message(message, new)

        except discord.NotFound:
            lfunc("Cannot edit message \"{}\", message not found".format(message.clean_content))
            if send_if_fail:
                lfunc("Sending message instead")
                return await self.safe_send_message(message.channel, new)

    async def safe_set_topic(self, channel, newmsg):
        try:
            await self.edit_channel(channel, topic=newmsg)
        except discord.Forbidden:
            log.warning('Could not set topic because of insufficient permissions.')
        except BaseException as e:
            log.warning('Could not set topic, detailed reason: %s' % (str(e)))

    async def send_typing(self, destination):
        try:
            return await super().send_typing(destination)
        except discord.Forbidden:
            log.warning("Could not send typing to {}, no permission".format(destination))

    async def wait_delete_msg(self, message, delay_in_seconds):
        await asyncio.sleep(delay_in_seconds)
        await self.safe_delete_message(message, quiet=True)
    # endregion

    # region Voice Channel Helper Functions
    @staticmethod
    def check_if_vc_is_empty(vchannel: discord.Channel, *, excluding_me=True, excluding_deaf=False):
        def check(member):
            if excluding_me and member == vchannel.server.me:
                return False

            if excluding_deaf and any([member.deaf, member.self_deaf]):
                return False

            return True

        return not sum(1 for m in vchannel.voice_members if check(m))

    @staticmethod
    async def check_ignore_non_voice(msg) -> bool:
        vc = msg.server.me.voice_channel

        # If we've connected to a voice chat and we're in the same voice channel
        if not vc or vc == msg.author.voice_channel:
            return True
        else:
            raise exceptions.PermissionsError(
                "You must be in the voice channel %s to use this command!" % vc.name
            )
    # endregion

    # region Voice Channel Actions
    async def set_voice_state(self, vchannel, *, mute=False, deaf=False):
        if isinstance(vchannel, discord.Object):
            vchannel = self.get_channel(vchannel.id)

        if getattr(vchannel, 'type', ChannelType.text) != ChannelType.voice:
            raise AttributeError('Channel passed must be a voice channel')

        await self.ws.voice_state(vchannel.server.id, vchannel.id, mute, deaf)

    async def get_voice_client(self, channel: discord.Channel):
        if isinstance(channel, discord.Object):
            channel = self.get_channel(channel.id)

        if getattr(channel, 'type', ChannelType.text) != ChannelType.voice:
            raise AttributeError('Channel passed must be a voice channel')

        if self.is_voice_connected(channel.server):
            return self.voice_client_in(channel.server)

        vc = None
        t0 = t1 = 0
        tries = 5

        for attempt in range(1, tries + 1):
            log.debug("Connection attempt {} to {}".format(attempt, channel.name))
            t0 = time.time()

            try:
                vc = await self.join_voice_channel(channel)
                t1 = time.time()
                break
            except asyncio.TimeoutError:
                log.warning("Failed to connect, retrying ({}/{} tries)".format(attempt, tries))
            except:
                log.exception("Unknown error attempting to connect to voice")

            await asyncio.sleep(0.5)

        if not vc:
            log.critical("Voice client is unable to connect, restarting...")
            await self.restart()

        log.debug("Connected in {:0.1f}s".format(t1 - t0))
        log.info("Connected to {}/{}".format(channel.server, channel))

        vc.ws._keep_alive.name = 'VoiceClient Keepalive'

        return vc

    async def join_voice_channel(self, channel):
        if isinstance(channel, discord.Object):
            channel = self.get_channel(channel.id)

        if getattr(channel, 'type', ChannelType.text) != ChannelType.voice:
            raise discord.InvalidArgument('Channel passed must be a voice channel')

        server = channel.server

        if self.is_voice_connected(server):
            raise discord.ClientException('Already connected to a voice channel in this server')

        def session_id_found(data):
            user_id = data.get('user_id')
            guild_id = data.get('guild_id')
            return user_id == self.user.id and guild_id == server.id

        # register the futures for waiting
        session_id_future = self.ws.wait_for('VOICE_STATE_UPDATE', session_id_found)
        voice_data_future = self.ws.wait_for('VOICE_SERVER_UPDATE', lambda d: d.get('guild_id') == server.id)

        # "join" the voice channel
        await self.ws.voice_state(server.id, channel.id)

        session_id_data = await asyncio.wait_for(session_id_future, timeout=15, loop=self.loop)

        data = await asyncio.wait_for(voice_data_future, timeout=15, loop=self.loop)

        kwargs = {
            'user': self.user,
            'channel': channel,
            'data': data,
            'loop': self.loop,
            'session_id': session_id_data.get('session_id'),
            'main_ws': self.ws
        }

        voice = discord.VoiceClient(**kwargs)
        try:
            log.voicedebug("Connecting to  Voice Channel \"%s\"..." % channel.name)
            with aiohttp.Timeout(15):
                await voice.connect()

        except asyncio.TimeoutError as e:
            log.voicedebug("Connection to \"%s\" failed, disconnecting!" % channel.name)
            try:
                await voice.disconnect()
            except:
                pass
            raise e

        log.voicedebug("Connection to \"%s\" successful" % channel.name)

        self.connection._add_voice_client(server.id, voice)
        return voice

    async def reconnect_voice_client(self, server, *, sleep=0.1, channel=None):
        log.debug("Reconnecting voice client on \"{}\"{}".format(
            server, ' to "{}"'.format(channel.name) if channel else ''))

        vc = self.voice_client_in(server)

        if not (vc or channel):
            return

        _paused = False
        player = self.get_player_in(server)

        if player and player.is_playing:
            player.pause()
            _paused = True

        log.voicedebug("Disconnecting from Voice Channel \"%s\" for reconnect" % channel.name)

        try:
            await vc.disconnect()
        except:
            pass

        if sleep:
            await asyncio.sleep(sleep)

        if player:
            if not channel:
                new_vc = await self.get_voice_client(vc.channel)
            else:
                new_vc = await self.get_voice_client(channel)
            await player.reload_voice(new_vc)

            if player.is_paused and _paused:
                player.resume()

        log.voicedebug("Reconnected voice client on \"{}\"{}".format(
            server,
            ' to "{}"'.format(channel.name) if channel else ''
        ))

    async def disconnect_voice_client(self, server):
        vc = self.voice_client_in(server)
        if not vc:
            return

        if server.id in self.players:
            self.players.pop(server.id).kill()

        await vc.disconnect()

    async def disconnect_all_voice_clients(self):
        for vc in list(self.voice_clients).copy():
            await self.disconnect_voice_client(vc.channel.server)
    # endregion
    ""
