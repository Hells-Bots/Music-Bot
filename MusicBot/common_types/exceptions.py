import shutil
import textwrap


class MusicbotException(Exception):
    """
    Base class for exceptions
    """
    def __init__(self, message):
        super().__init__(message)
        self._message = message

    @property
    def message(self):
        return self._message

    @property
    def message_no_format(self):
        return self._message


class CommandError(MusicbotException):
    """
    Exceptions for when something went wrong during the processing of a command
    """
    pass


class ExtractionError(MusicbotException):
    """
    Exception for when something went wrong during the processing of a song/ytdl stuff
    """
    pass


class WrongEntryTypeError(ExtractionError):
    """
    Exception for when a no processing entry type failed and an entry was a playlist/vice versa
    """
    def __init__(self, message, is_playlist, use_url):
        super().__init__(message)
        self.is_playlist = is_playlist
        self.use_url = use_url


class FFmpegError(MusicbotException):
    """
    Exception for when FFmpeg complained about something critical
    """
    pass


class FFmpegWarning(MusicbotException):
    """
    Exception for when FFmpeg complained about something ignorable
    """
    pass


class PermissionsError(CommandError):
    """
    Exception for when the user has insufficient permisions for using the command
    """
    @property
    def message(self):
        return "You don't have permission to use that command.\nReason: " + self._message


class HelpfulError(MusicbotException):
    """
    Prettily formatted Error which also provides possible solutions for the user
    """
    def __init__(self, issue, solution, *, preface="An error has occured:", footnote='', expire_in=0):
        self.issue = issue
        self.solution = solution
        self.preface = preface
        self.footnote = footnote
        self.expire_in = expire_in
        self._message_fmt = "\n{preface}\n{problem}\n\n{solution}\n\n{footnote}"

    @property
    def message(self):
        return self._message_fmt.format(
            preface  = self.preface,
            problem  = self._pretty_wrap(self.issue,    "  Problem:"),
            solution = self._pretty_wrap(self.solution, "  Solution:"),
            footnote = self.footnote
        )

    @property
    def message_no_format(self):
        return self._message_fmt.format(
            preface  = self.preface,
            problem  = self._pretty_wrap(self.issue,    "  Problem:", width=None),
            solution = self._pretty_wrap(self.solution, "  Solution:", width=None),
            footnote = self.footnote
        )

    @staticmethod
    def _pretty_wrap(text, pretext, *, width=-1):
        if width is None:
            return '\n'.join((pretext.strip(), text))
        elif width == -1:
            pretext = pretext.rstrip() + '\n'
            width = shutil.get_terminal_size().columns

        lines = textwrap.wrap(text, width=width - 5)
        lines = (('    ' + line).rstrip().ljust(width-1).rstrip() + '\n' for line in lines)

        return pretext + ''.join(lines).rstrip()


class Signal(Exception):
    """
    Base Class for Lifecycle controlling signals
    """
    pass


class RestartSignal(Signal):
    """
    Signal to trigger a restart of the bot
    """
    pass


class TerminateSignal(Signal):
    """
    Signal to trigger a graceful exit of the bot
    """
    pass
