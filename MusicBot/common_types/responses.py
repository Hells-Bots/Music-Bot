class Response:
    __slots__ = ['_content', 'reply', 'delete_delay_multiplicator', 'codeblock', '_codeblock']

    def __init__(self, content, reply=False, delete_delay_multiplicator=1, codeblock=None):
        self._content = content
        self.reply = reply
        self.delete_delay_multiplicator = delete_delay_multiplicator
        self.codeblock = codeblock
        self._codeblock = "```{!s}\n{{}}\n```".format('' if codeblock is True else codeblock)

    @property
    def content(self):
        if self.codeblock:
            return self._codeblock.format(self._content)
        else:
            return self._content


class AnimatedResponse(Response):
    def __init__(self, content, *sequence, delete_delay_multiplicator=1):
        super().__init__(content, delete_delay_multiplicator=delete_delay_multiplicator)
        self.sequence = sequence
